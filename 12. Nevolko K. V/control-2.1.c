#include<stdio.h>
#include<math.h>
int main()
{
float a,b,c,h,S,P,al,bl,cl;
printf("Введите угол A\nВысоту\nСторону C\n");scanf("%f%f%f",&al,&h,&c);
if ((al<0) || (h<0) || (c<0))
{
	printf("Пожалуйста введите не отрицательное(ые) значение(я)\n");
}
else
{
	b=h/(sin(al)*cos(al));
    S=(b*h)/2;
    a=sqrt(b*b+c*c-2*b*c*cos(al));
    P=a+c+b;
    cl=sin(h/a);
    bl=3.14-(al+cl);
    printf("Сторона b = %f;\nСторона a = %f;\nУгол b в радианах = %f;\nУгол c в радианах = %f;\nПлощадь = %f;\nПериметр = %f\n",b,a,bl,cl,S,P);
}
return 0;	
}
