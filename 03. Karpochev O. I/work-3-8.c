#include <stdio.h>
int main() {
  int k;  // объявляем целую переменную key
  printf("k = ");
  scanf("%d", &k);   // вводим значение переменной k
  for(int i=1, j=2; i<=k; i++, j+=2) // цикл для переменных
  {                                  // (i от 1 до k с шагом 1) и (j от 2 с шагом 2)
    printf("i = %d   j = %d\n", i, j); // выводим значения i и j
  }
  getchar(); getchar();
  return 0;
}