/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/
#include <stdio.h>
#include <math.h>
int main()
{

    double A=0, B=0, k=0;
    double C=0, e=0, f=0, g=0, H=1.57, P, S;
    input:
    printf("Введите углы A и B(в градусах) и высоту:\n");
    printf("Введите угол A: "); scanf("%lf",&A);
    printf("Введите угол B: "); scanf("%lf",&B);
    printf("Введите высоту h: "); scanf("%lf",&k);
    
    A = A * 3.14/180;
    B = B * 3.14/180;
    if ((A+B)>=3.14) 
    {
        printf("Ошибка. \n Введите данные заново:\n");
        goto input; 
    }
    C = 3.14 - ( B + H );
    e = ( k / sin(B) );
    C = 3.14 - ( A + B );
    f = e * ( sin(B) / sin(A) );
    g = e * ( sin(C) / sin(A) );
    P = e + f + g;
    S = 0.5 * (k * g);
    A = A * 180/3.14;
    B = B * 180/3.14;
    C = C * 180/3.14;
    
    printf("Элементы треугольника:\n");
    printf("Сторона a=%lf\n", e);
    printf("Сторона b=%lf\n", f);
    printf("Сторона c=%lf\n", g);
    printf("Угол A=%lf\n", A);
    printf("Угол B=%lf\n", B);
    printf("Угол С=%lf\n", C);
    printf("Периметр =%lf\n", P);
    printf("Площадь =%lf СМ^2\n", S);
    return 0;
}

