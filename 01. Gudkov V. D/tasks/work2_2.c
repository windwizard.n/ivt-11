#include<stdio.h>
#include<math.h>

/* #define PI 3.14159265359 */

double y(double);

int main()
{
  double x = 0;
  printf("Enter x: ");
  scanf("%lf",&x);
  double yy = y(x);
  if(yy == -1){
    printf("Function y is not defined for %lf\n",x);
    return 0;
  }
  printf("x = %lf\n",y(x));
  return 0;
}

double y(double x)
{
  /* x = (PI/180)*x; */
  if(x > 1 && x < 2)
    return pow(cos(x),2);
  else if(x<=0 || x>=2)
    return 1+pow(sin(x),2);
  else
    return -1;
}
