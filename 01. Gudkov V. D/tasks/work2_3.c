#include<stdio.h>
#include<math.h>

double s(double,int);

int main()
{
  double x = 0;
  int n = 0;
  printf("Please, enter x: ");
  scanf("%lf",&x);
  if(x==0){
    printf("Error! division by zero!\n");
    return 0;
  }
  printf("Please, enter n: ");
  scanf("%d",&n);
  printf("Result: %lf\n",s(x,n));
  return 0;
}

double s(double x, int n)
{
  double r = 0;
  int k = 1;
  while(n--){
    r+=pow(-1,k-1)*((pow(k,2)+k+1)/pow(x,k));
    ++k;
  }
  return r;
}
