#include<stdio.h>
#include<math.h>

#define PI 3.14159265359

void find_sas(double,double,double,double*,double*,double*);
double triangle_p(double,double,double);
double triangle_sp(double);
double triangle_area(double,double,double,double);
double triangle_h(double,double);
double triangle_inr(double,double);
double triangle_cr(double,double,double,double);

int main()
{
  double a,b,c,A,B,C;
  printf("Hello, I will find two angles and side of a triangle.\n");
  printf("Please, enter known side a: ");
  scanf("%lf",&a);
  if(a<=0){
    printf("Side a must be greater than zero!\n");
    return 0;
  }
  printf("Please, enter known side b: ");
  scanf("%lf",&b);
  if(b<=0){
    printf("Side b must be greater than zero!\n");
    return 0;
  }
  printf("Please, enter known angle between a and b: ");
  scanf("%lf",&A);
  if(A<=0){
    printf("Angel must be greater than zero!\n");
    return 0;
  }
  find_sas(a,b,A,&c,&B,&C);
  double perimeter = triangle_p(a,b,c);
  double semiperimeter = triangle_sp(perimeter);
  double area = triangle_area(semiperimeter,a,b,c);
  double ha = triangle_h(area,a);
  double hb = triangle_h(area,b);
  double hc = triangle_h(area,c);
  double inradius = triangle_inr(area,semiperimeter);
  double circumradius = triangle_cr(a,b,c,area);
  printf("%8s\n","Triangle:");
  printf("%-14s %5.3lf\n","Side a: ",a);
  printf("%-14s %5.3lf\n","Side b: ",b);
  printf("%-14s %5.3lf\n","Side c: ",c);
  putchar('\n');
  printf("%-14s %5.3lf\n","Angle A: ",A);
  printf("%-14s %5.3lf\n","Angle B: ",B);
  printf("%-14s %5.3lf\n","Angle C: ",C);
  putchar('\n');
  printf("%-14s %5.3lf\n","Height a: ",ha);
  printf("%-14s %5.3lf\n","Height b: ",hb);
  printf("%-14s %5.3lf\n","Height c: ",hc);
  putchar('\n');
  printf("%-14s %5.3lf\n","Perimetr: ",perimeter);
  printf("%-14s %5.3lf\n","Area: ",area);
  putchar('\n');
  printf("%-14s %5.3lf\n","Inradius: ",inradius);
  printf("%-14s %5.3lf\n","Circumradius: ",circumradius);
  return 0;
}

void find_sas(double a,double b,double A,double* c,double* B,double* C)
{
  /* a,b,c - sides and A,B,C - angles of triangle */
  double cos_A = cos((PI/180)*A);
  *c = sqrt(a*a+b*b-2*a*b*cos_A);
  *B = (acos((b*b+(*c)*(*c)-a*a)/(2*b*(*c))))*(180/PI);
  *C = 180 - (*B) - A;
}

double triangle_p(double a,double b,double c)
{
  return a+b+c;
}
double triangle_sp(double p)
{
  return p/2;
}
double triangle_area(double s,double a, double b,double c)
{
  return sqrt(s*(s-a)*(s-b)*(s-c));
}
double triangle_h(double area,double side)
{
  return (2*area)/side;
}
double triangle_inr(double area,double s)
{
  return area/s;
}
double triangle_cr(double a,double b,double c, double area)
{
  return (a*b*c)/(4*area);
}
