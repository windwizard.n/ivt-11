//Студент: Гудков Вячеслав Дмитриевич
#include<stdio.h>
#include<unistd.h>

int main()
{
  int d;
  d = 0;
  long double e;
  e = 0;
  while(1){
    int fac, i;
    fac = d;
    i = d;
    if(!i || i == 1)
      fac = i = 1;
    while(--i)
      fac*=i;
    e+=1.0/fac;
    ++d;
    printf("e --> %.50Lf\n", e);
    usleep(100000);
  }
}
