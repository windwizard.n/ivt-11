/* Студент: Гудков Вячеслав Дмитриевич */
#include<stdio.h>

int main()
{
 int i, neg, pos, zero;
 double num;
 i = neg = pos = zero = 0;
 num = 0;
 while(i != 5){
   printf("Enter number: ");
   scanf("%lf",&num);
   if(num > 0)
     ++pos;
   else if(num < 0)
     ++neg;
   else
     ++zero;
   ++i;
 }
 printf("Number of negatives: %d\nNumber of positives: %d\nNumber of zeros: %d\n", neg, pos, zero);
 return 0;
}
