#include <stdio.h>
#include <math.h>

double s(int);
int main()
{
    int n=0;
    printf("Введите число n: ");
    scanf("%d",&n);
    printf("Ответ %lf\n",s(n));
    return 0;
}
double s(int n)
{
  double b = 0;
  int k = 0;
  while(n--)
  {
    b+=pow(-2,k)/(pow(k,2)+1);
    ++k;
  }
  return b;
}
