#include <stdio.h>
#include <math.h>
int main()
{
    int a,b,g;
    float s1,s2,s3;
	printf("Введите 1-й угол\n");
    scanf("%d",&a);
    printf("Введите 2-й угол\n");
    scanf("%d",&b);
    printf("Введите сторону треугольника\n");
    scanf("%f",&s1);
    if(a==90 & b==90)
    {
        printf ("В треугольнике не может быть два угла равных 90 градусов");
        return 0;
    }
     if((a+b)>=180)
    {
        printf ("В треугольнике не может быть два угла,сумма которых равна больше 180 градусов");
        return 0;
    }
    if(a<=0||b<=0||s1<=0)
    {
        printf ("Вы ввели неправильные значения.");
        return 0;
    }
    g=180-a-b;
    printf("Третий угол равен %d\n",g);
    s2=s1*(sin(b*M_PI/180)/sin(a*M_PI/180));
    printf("Второая сторона равна %.2f\n",s2);
    s3=s1*(sin(g*M_PI/180)/sin(a*M_PI/180));
    printf("Третья сторона равна  %.2f\n",s3);
    printf("Периметр равен %.2f\n",s1+s2+s3);
    printf("Площадь равна %.3f\n",sin(a*M_PI/180)*s2*s3/2);
    printf("Высота равна %.2f\n",sin(a*M_PI/180)*s1*s2/s1);
	return 0;
}