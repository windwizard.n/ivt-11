#include<stdio.h>
#include<math.h>
#define PI 3.14159255359

double fside(double, double);
double flast(double, double);
double perim(double, double, double);
double area(double, double, double, double);
double high(double, double);
double inrd(double, double);

int main()
{
	double A, B, R;
	printf("Введите угол А\n");
	scanf("%lf", &A);
	printf("Введите угол Б\n");
	scanf("%lf", &B);
	printf("Введите радиус\n");
	scanf("%lf", &R);
	double a = fside(R, A);
	double b = fside(R, B);
	printf("\n%lf - сторона А\n", a);
	printf("%lf - сторона Б\n", b);
	double C = flast(A, B);
	printf("%lf - угол Ц\n", C);
	double c = fside(R, C);
	printf("%lf - сторона Ц\n", c);
	double p = perim(a, b, c);
	printf("%lf - периметр\n", p);
	double ar = area(a, b, c, p);
	printf("%lf - площадь\n", ar);
	double ha = high(ar, a);
	double hb = high(ar, b);
	double hc = high(ar, c);
	printf("%lf - высота ha\n%lf - высота hb\n%lf - высота hc\n", ha, hb, hc);
	double inrad = inrd(ar, p);
	printf("%lf - радиус вписанной окружности\n", inrad);
	return 0;
}

double fside(double radius, double angle)
{
	return (2 * radius) * sin(((PI / 180) * angle));
}

double flast(double A, double B)
{
	return 180 - (A + B);
}

double perim(double a, double b, double c)
{
	return a + b + c;
}

double area(double a, double b, double c, double p)
{
	double sp = p / 2;
	return sqrt(sp * (sp - a) * (sp- b) * (sp - c));
}

double high(double ar, double side)
{
	return (2 * ar) / side;
}

double inrd(double area, double prmt)
{
	return area / (prmt / 2);
}
