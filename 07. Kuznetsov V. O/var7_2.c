#include<stdio.h>
#include<math.h>

double f(double);

int main()
{
	double x;
	printf("Введите число\n");
	scanf("%lf", &x);
	printf("Ответ: %lf\n", f(x));
	return 0;
}

double f(double x)
{
	//printf("%lf\n", x);
	if(x >= 0)
		return exp(-x);
	else
		return cos(x);
}
